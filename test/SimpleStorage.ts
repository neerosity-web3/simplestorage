import { assert } from 'chai';
import { ethers } from 'hardhat';
import { SimpleStorage, SimpleStorage__factory } from '../typechain-types';

describe('SimpleStorage', () => {
    let simpleStorageFactory: SimpleStorage__factory,
        simpleStorage: SimpleStorage;

    beforeEach(async () => {
        simpleStorageFactory = (await ethers.getContractFactory(
            'SimpleStorage'
        )) as SimpleStorage__factory;
        simpleStorage = await simpleStorageFactory.deploy();
    });

    it('should start with a favorite number of 0', async () => {
        const currentValue = await simpleStorage.retrieve();
        const expectedValue = '0';

        assert.equal(currentValue.toString(), expectedValue);
        // expect(currentValue.toString()).to.equal(expectedValue);
    });

    it('should update when we call store', async () => {
        const expectedValue = '7';
        const transactionResponse = await simpleStorage.store(expectedValue);
        await transactionResponse.wait();

        const currentValue = await simpleStorage.retrieve();
        assert.equal(currentValue.toString(), expectedValue);
    });
});
