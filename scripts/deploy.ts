import { ethers, network, run } from 'hardhat';

// async main
async function main() {
    const SimpleStorageFactory = await ethers.getContractFactory(
        'SimpleStorage'
    );
    console.log('Deploying contract');
    const simpleStorage = await SimpleStorageFactory.deploy();
    await simpleStorage.deployed();
    console.log(`Deployed contract to: ${simpleStorage.address}`);
    if (network.config.chainId === 80001 && process.env.POLYSCAN_API_KEY) {
        await simpleStorage.deployTransaction.wait(4);
        await verify(simpleStorage.address, []);
    }
    {
        console.log("Can't run verify on local network");
    }

    const currentValue = await simpleStorage.retrieve();
    console.log(`Current Value is: ${currentValue}`);

    // Update current value
    const transactonResponse = await simpleStorage.store(9);
    await transactonResponse.wait(1);

    const updatedValue = await simpleStorage.retrieve();
    console.log(`Updated Value is: ${updatedValue}`);
}

async function verify(contractAddress: string, args: any) {
    console.log('Verifying contract...');
    try {
        await run('verify:verify', {
            address: contractAddress,
            constructorArguments: args,
        });
    } catch (e) {
        console.log(e);
    }
}

// main
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.log(error);
        process.exit(1);
    });
