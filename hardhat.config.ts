import '@nomicfoundation/hardhat-toolbox';
import '@nomiclabs/hardhat-etherscan';
import * as dotenv from 'dotenv';
import { HardhatUserConfig } from 'hardhat/config';
import 'solidity-coverage';
import './tasks/block-number';

dotenv.config();

const MUMBAI_RPC_URL = process.env.MUMBAI_RPC_URL || '';
const PRIVATE_KEY = (process.env.PRIVATE_KEY as string) || '';
const POLYSCAN_API_KEY = process.env.POLYSCAN_API_KEY || '';

const config: HardhatUserConfig = {
    solidity: '0.8.17',
    defaultNetwork: 'hardhat',
    networks: {
        mumbai: {
            url: MUMBAI_RPC_URL,
            accounts: [PRIVATE_KEY],
            chainId: 80001,
        },
        localhost: {
            url: 'http://127.0.0.1:8545/',
            chainId: 31337,
        },
    },
    etherscan: {
        apiKey: POLYSCAN_API_KEY,
    },
    gasReporter: {
        enabled: false,
        outputFile: 'gas-report.txt',
        noColors: true,
        // coinmarketcap: <API>,
        token: 'MATIC',
    },
};

export default config;
